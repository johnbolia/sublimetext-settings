import sublime
import sublime_plugin


class MultiIndentCommand(sublime_plugin.TextCommand):
    def run(self, edit):
        view = self.view
        selections = view.sel()
        for selection in selections:
            line_upto_cursor = view.substr(
                sublime.Region(view.line(selection).a, selection.b))
            param_column = self.find_last_unmatched_open_paren(line_upto_cursor)
            whitespace_region = self.expand_to_whitespace(selection.a)
            view.erase(edit, whitespace_region)
            if param_column:
                view.insert(edit, whitespace_region.a, '\n%s' % (' ' * param_column))
            else:
                view.run_command('insert', {'characters': '\n'})

    def find_last_unmatched_open_paren(self, line):
        '''Returns offset of the last unmatched opening parenthesis on the line'''
        char_dict = {'(': ')', '[': ']', '{': '}', '  ': ''}  # I do not think '   ' is working
        line_length = len(line)
        closing_paren = 0
        while line_length > 0:
            line_length -= 1
            char = line[line_length]
            for leader, trailer in char_dict.items():
                if char is trailer:
                    closing_paren += 1
                elif char is leader:
                    if closing_paren > 0:
                        closing_paren -= 1
                    else:
                        # Get index beneath the opening parenthesis.
                        return line_length + 1
        return None

    def expand_to_whitespace(self, point):
        '''Returns region with all spaces matched before and after the point'''
        view = self.view
        line_upto_point = view.substr(sublime.Region(view.line(point).a, point))
        whitespace_to_eat = 0
        last_non_ws_index = len(line_upto_point)
        while last_non_ws_index > 0:
            last_non_ws_index -= 1
            char = line_upto_point[last_non_ws_index]
            if char != ' ':
                break
            whitespace_to_eat += 1
        return view.find(' *', point - whitespace_to_eat)

''' key binding:
[
  {"keys": ["enter"], "command": "multi_indent",
   "context": [
     {"key": "setting.translate_tabs_to_spaces"},
     {"key": "preceding_text", "operator": "regex_contains", "operand": "\\([^(]+,\\s*$|\\[[^\\[]+,\\s*$|{[^{]+,\\s*$", "match_all": false}
   ]
  }
]
'''
